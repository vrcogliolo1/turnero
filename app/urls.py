from django.urls import path
from app import views

app_name = 'app'

urlpatterns = [
    path('', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('gestion/', views.gestion, name='gestion'),
    path('gestion/<int:id>', views.gestion, name='gestion'),
    path('gestion/<int:id>', views.gestion, name='gestion'),
    path('gestion/<int:id>/<int:q>', views.gestion, name='gestion'),
    path('eliminar_cliente', views.eliminar_cliente, name='eliminar'),
    path('editar_cliente/<int:cliente_id>', views.editar_cliente, name='editar_cliente'),
]