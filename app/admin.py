from django.contrib import admin
from .models import Servicio, Cliente

admin.site.register(Servicio)
admin.site.register(Cliente)
# Register your models here.
