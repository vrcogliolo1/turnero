import random
import string
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.http import JsonResponse

from .models import Servicio, Cliente
from .forms import ClienteForm

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('app:gestion')
        else:
            messages.error(request, 'Credenciales inválidas. Por favor, intenta de nuevo.')
    return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return redirect('app:login')

def gestion(request,id=None):
    servicios = Servicio.objects.all()
    clientes = None
    if id:
        clientes = Cliente.objects.filter(servicio=id).order_by('prioridad', 'id')
    if request.method == 'POST':
        form = ClienteForm(request.POST)
        if form.is_valid():
            cliente = form.save(commit=False)
            existe = Cliente.objects.filter(cedula=cliente.cedula).first()
            if existe:
                existe.servicio = cliente.servicio
                existe.prioridad = cliente.prioridad
                cliente = existe
            else:
                cliente.ticket = generar_ticket_unico(cliente)
            cliente.save()
    
    form = ClienteForm()
    return render(
        request,
        'servicio.html',
        {
            'servicios':servicios,
            'clientes':clientes,
            'form': form,  
        }
    )

def generar_ticket_unico(cliente):
    cedula = str(cliente.cedula)
    longitud = 4 
    combinacion = generar_combinacion_aleatoria(longitud)
    ticket = cedula[:2] + combinacion
    if len(Cliente.objects.filter(ticket = ticket)) != 0:
        ticket = generar_ticket_unico(cliente)
    return ticket

def eliminar_cliente(request):
    if request.method == 'POST':
        cliente_id = request.POST.get('cliente_id')
        if cliente_id:
            # Obtener el cliente a eliminar a partir de su ID
            cliente = get_object_or_404(Cliente, id=cliente_id)
            cliente.delete()
            return JsonResponse({'status': 'success', 'message': 'Cliente eliminado exitosamente.'})
        else:
            return JsonResponse({'status': 'error', 'message': 'ID del cliente no proporcionado.'})
    else:
        return JsonResponse({'status': 'error', 'message': 'Método de solicitud no válido.'})
    
def generar_combinacion_aleatoria(longitud):
    caracteres = string.ascii_letters + string.digits
    combinacion = ''.join(random.choice(caracteres) for _ in range(longitud))
    return combinacion


def editar_cliente(request, cliente_id):
    cliente = get_object_or_404(Cliente, id=cliente_id)
    form = ClienteForm(instance=cliente)
    return render(request, 'editar_cliente.html', {'form': form, 'cliente': cliente})