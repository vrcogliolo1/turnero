from django.db import models

class Servicio(models.Model):
    servicio = models.CharField(max_length=100)
    def __str__(self):
        return self.servicio

class Cliente(models.Model):
    class Categoria(models.TextChoices):
        ALTA = 'A','3ra Edad, Embarazadas, Discapacidad'
        MEDIA = 'B','Clientes corporativos'
        BAJA = 'C','Clientes personales'
    
    cedula = models.IntegerField()
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    prioridad = models.CharField(choices=Categoria.choices,default=Categoria.BAJA)
    ticket = models.CharField()

    def __str__(self):
        return f"Cliente: {self.cedula}, Servicio: {self.servicio}, Prioridad: {self.prioridad}, Ticket: {self.ticket}"
# Create your models here.
